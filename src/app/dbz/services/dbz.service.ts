import { Injectable } from '@angular/core';
import { Character } from '../interfaces/character.interface';

@Injectable()
export class DbzService {
  private _chars: Character[] = [
    { name: 'Goku', power: 60000 },
    { name: 'Vegeta', power: 50000 },
  ];

  get chars(): Character[] {
    return [...this._chars];
  }

  constructor() {}

  addNewCharacter(character: Character) {
    this._chars.push(character);
  }
}
