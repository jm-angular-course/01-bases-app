import { DbzService } from './../services/dbz.service';
import { Component, Input } from '@angular/core';
import { Character } from '../interfaces/character.interface';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
})
export class CharactersComponent {
  // @Input()
  // chars!: Character[];
  headerColor: string = 'blue';

  get chars(): Character[] {
    return this.dbzService.chars;
  }

  constructor(private dbzService: DbzService) {}
}
