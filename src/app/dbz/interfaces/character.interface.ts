export interface Character {
  name: string;
  power: number;
}

export const initChar = {
  name: '',
  power: 0,
};
