import { DbzService } from './../services/dbz.service';
import { Component, Input } from '@angular/core';
import { Character } from '../interfaces/character.interface';

@Component({
  selector: 'app-add-new',
  templateUrl: './add-new.component.html',
})
export class AddNewComponent {
  @Input()
  char!: Character;

  // @Output()
  // onNewChar: EventEmitter<Character> = new EventEmitter();

  constructor(private dbzService: DbzService) {}

  add() {
    if (!this.char.name || !this.char.power) {
      alert('Ingrese un nombre y nivel de poder válidos');
      return;
    }
    // this.onNewChar.emit(this.char);
    this.dbzService.addNewCharacter(this.char);
    this.char = { name: '', power: 0 };
  }

  handleChangePower(event: any) {
    this.char.power = event.target.value;
  }
}
