import { DbzService } from './../services/dbz.service';
import { Component } from '@angular/core';
import { Character, initChar } from '../interfaces/character.interface';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
})
export class MainPageComponent {
  char: Character = { name: 'Maestro Roshi', power: 100 };

  // get chars(): Character[] {
  //   return this.dbzService.chars;
  // }

  constructor(private dbzService: DbzService) {}
}
