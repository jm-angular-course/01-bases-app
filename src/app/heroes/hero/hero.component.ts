import { Component } from '@angular/core';

@Component({
  selector: 'app-hero',
  templateUrl: 'hero.component.html',
})
export class HeroComponent {
  name: string = 'Superman';
  age: number = 50;

  setName(name: string): void {
    this.name = name;
  }
  setAge(age: number): void {
    this.age = age;
  }

  toString(): string {
    return `${this.name} - ${this.age}`;
  }
}
