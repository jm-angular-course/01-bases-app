import { Component } from '@angular/core';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
})
export class ListadoComponent {
  heroes: string[] = ['Superman', 'Spiderman', 'Batman'];
  deletedHeroes: string[] = [];

  constructor() {
    console.log('constructor');
  }

  deleteHero() {
    const deletedHero = this.heroes.pop();
    if (deletedHero) {
      this.deletedHeroes.push(deletedHero);
    }
  }
}
