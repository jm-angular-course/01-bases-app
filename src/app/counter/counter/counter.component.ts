import { Component } from '@angular/core';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
})
export class CounterComponent {
  base: number = 5;
  counter: number = this.base;

  public acc(value: Number) {
    if (value > 0) {
      this.counter = this.counter + this.base;
    } else {
      this.counter = this.counter - this.base;
    }
  }
}
