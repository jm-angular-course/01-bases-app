import { CommonModule } from '@angular/common';
import { CounterComponent } from './counter/counter.component';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [CounterComponent],
  exports: [CounterComponent],
  imports: [CommonModule],
  providers: [],
})
export class CounterModule {}
